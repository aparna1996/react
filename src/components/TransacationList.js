import React, { useContext } from 'react';
import { GlobalContext } from "../context/GlobalState";
import Transacation from './Transacation';

const TransacationList = () => {
  const { transactions } = useContext(GlobalContext);

  return (
    <>
      <h3>History</h3>
      <ul id="list" className="list">
        {transactions.map(transaction => (<Transacation key={transaction.id} transaction={transaction}/>))}

      </ul>
    </>
  )
}

export default TransacationList

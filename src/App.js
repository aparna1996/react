import './App.css';
import {Header} from "./components/Header"
import {Balance} from "./components/Balance";
import IncomeExpenses from "./components/incomeExpenses"
import TransacationList from './components/TransacationList';
import AddTransaction from './components/AddTransaction';
import  {GlobalProvider} from "./context/GlobalState";

function App() {
  return (
    <GlobalProvider>
    <Header/>
     <div className='container'>
      <Balance/>
      <IncomeExpenses/>
      <TransacationList/>
      <AddTransaction/>
     </div>
    </GlobalProvider>
  );
}

export default App;
